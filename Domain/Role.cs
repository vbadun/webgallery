﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebGallery.Domain
{
    public class Role : IRole<Guid>
    {
        public Role()
        {
            this.Id = Guid.NewGuid();
        }

        public Role(string name)
            : this()
        {
            this.Name = name;
        }

        public Role(string name, Guid id)
        {
            this.Name = name;
            this.Id = id;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
