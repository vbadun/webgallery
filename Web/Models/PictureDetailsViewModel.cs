﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class PictureDetailsViewModel

    {
        public string Name { get; set; }

        public string CategoryName { get; set; }
        public string CommentCreate { get; set; }
        public string PathInfo { get; set; }
        public Guid Id { get; set; }

        public int Like { get; set; }

        public int DisLike { get; set; }

        public List<CommentEntity> Comments { get; set; }

               

    }
}