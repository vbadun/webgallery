﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class CategoryList
    {
        
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int PicturesCount { get; set; }
        public ICollection<PictureEntity> Pictures { get; set; }
    }
}