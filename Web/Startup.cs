﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebGallery.Web.Startup))]
namespace WebGallery.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
