﻿using System.Web.Mvc;
using Unity.Mvc5;
using Microsoft.AspNet.Identity;
using System;
using Unity;
using DataAccess;
using Web.Identity;
using Unity.Lifetime;
using Unity.Injection;

namespace WebGallery.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IUnitOfWork, UnitOfWork>(new HierarchicalLifetimeManager(), new InjectionConstructor("WebGallery"));
        //    container.RegisterType<IUserStore<IdentityUser, Guid>, UserStore>(new TransientLifetimeManager());
            container.RegisterType<RoleStore>(new TransientLifetimeManager());
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}