﻿using DataAccess.Entities;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Web.Mvc;
using Web.Models;
using System.Threading.Tasks;
using PagedList;
using PagedList.Mvc;
using ExpressMapper.Extensions;
using System.Net.Http;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPictureService _pictureService;
        private readonly ICategoryService _categoryService;
        private readonly IRatingService _ratingService;
        private readonly ICommentService _commentService;
        public HomeController()
        {
            _pictureService = new PictureService();
            _categoryService = new CategoryService();
            _ratingService = new RaitingService();
            _commentService = new CommentService();
        }

        public ActionResult Index(int? page)
        {
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            var listpict = Mapper.Map<IEnumerable<PictureEntity>, List<ViewPictureModel>>(_pictureService.GetAll());
            return View(listpict.ToPagedList(pageNumber, pageSize));
        }
        public async Task<ActionResult> IndexByCategoriesAsync(int? page)
        {
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            List<PicturesByCategory> res = await _pictureService.GetByCount(5);
            //  var listpict = Mapper.Map<List<PicturesByCategory>, List<ViewPictureModel>>(_pictureService.GetByCount(3).Result);
            return View(res);
        }



        [HttpGet]
        public ActionResult CategoriesList()
        {
            var catlist = Mapper.Map<IEnumerable<CategoryEntity>, List<CategoryList>>(_categoryService.GetAll());

            return View(catlist);
        }

        [HttpGet]
        public ActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCategory(CategoryEntity category)
        {
            var cat = new CategoryEntity() { Id = Guid.NewGuid(), Name = category.Name };
            _categoryService.Add(cat);
            return RedirectToAction("CategoriesList");
        }

        [HttpGet]
        public ActionResult ShowPictureDetail(Guid id)
        {
            var pict = _pictureService.Get(id);
            return View(pict);
        }

        [HttpPost]
        public ActionResult ShowPictureDetailByModel(PictureDetailsViewModel message)
        {
            var comment = new CommentEntity()
            {
                Id = Guid.NewGuid(),
                Comment = message.CommentCreate,
                DateTime = DateTime.Now,
                HostAdress = Server.HtmlEncode(Request.UserHostAddress),
                PictureEntityId = message.Id
            };
            _commentService.Add(comment);
            return RedirectToAction("ShowPictureDetailByModel");
        }
        [HttpGet]
        public ActionResult ShowPictureDetailByModel(Guid id)
        {
            var pict = _pictureService.Get(id);
            var model = new PictureDetailsViewModel()
            {
                Name = pict.Name,
                CategoryName = pict.CategoryEntity.Name,
                Comments = _commentService.GetAllByPictureId(id).ToList(),
                Like = (int)_ratingService.GetByPictureId(id).Select(x => x.Like).Sum(),
                DisLike = (int)_ratingService.GetByPictureId(id).Select(x => x.DisLike).Sum(),
                PathInfo = pict.PathInfo
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var pict = _pictureService.Get(id);
            return View(pict);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload, PictureEntity picture)
        {
            if (upload != null)
            {
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                var id = Guid.NewGuid();
                string path = "~/content/" + id.ToString() + fileName;
                upload.SaveAs(Server.MapPath(path));
                var pic = new PictureEntity() { Id = id, Name = picture.Name, PathInfo = path, CategoryEntityId = picture.CategoryEntity.Id };
                _pictureService.Add(pic);
            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Upload()
        {
            SelectList categories = new SelectList(_categoryService.GetAll().ToList(), "Id", "Name");
            ViewBag.Categories = categories;
            return View();
        }
        [HttpGet]
        public ActionResult DeletePicture(Guid id)
        {
            var pict = _pictureService.Get(id);
            _pictureService.Delete(id);
            return RedirectToAction("Index");
        }


        public ActionResult Like(Guid id)
        {
            var adress = Server.HtmlEncode(Request.UserHostAddress);
            var rateimage = _ratingService.GetByPictureId(id);
            if (rateimage.Select(x => x.HostAdress).Contains(adress))
            {
                if (rateimage.Where(x => x.HostAdress == adress).Select(x => x.Like).Sum() >= 1)
                {
                    var rating = new RaitingEntity() { Id = Guid.NewGuid(), Like = -1, PictureEntityId = id, HostAdress = adress };
                    _ratingService.Add(rating);
                }
                else
                {
                    var rating = new RaitingEntity() { Id = Guid.NewGuid(), Like = 1, PictureEntityId = id, HostAdress = adress };
                    _ratingService.Add(rating);
                }
            }
            else
            {
                var rating = new RaitingEntity() { Id = Guid.NewGuid(), Like = 1, PictureEntityId = id, HostAdress = adress };
                _ratingService.Add(rating);
            }
            return RedirectToAction("Index");
        }
        public ActionResult DisLike(Guid id)
        {
            var adress = Server.HtmlEncode(Request.UserHostAddress);
            var rateimage = _ratingService.GetByPictureId(id);
            if (rateimage.Select(x => x.HostAdress).Contains(adress))
            {
                if (rateimage.Where(x => x.HostAdress == adress).Select(x => x.DisLike).Sum() <= -1)
                {
                    var rating = new RaitingEntity() { Id = Guid.NewGuid(), DisLike = 1, PictureEntityId = id, HostAdress = adress };
                    _ratingService.Add(rating);
                }
                else
                {
                    var rating = new RaitingEntity() { Id = Guid.NewGuid(), DisLike = -1, PictureEntityId = id, HostAdress = adress };
                    _ratingService.Add(rating);
                }
            }
            else
            {
                var rating = new RaitingEntity() { Id = Guid.NewGuid(), DisLike = -1, PictureEntityId = id, HostAdress = adress };
                _ratingService.Add(rating);
            }

            return RedirectToAction("Index");

        }


        public ActionResult CategoryDetails(Guid id)
        {
            var listpict = Mapper.Map<IEnumerable<PictureEntity>, List<ViewPictureModel>>(_pictureService.GetAll().Where(x => x.CategoryEntityId == id));
            return View(listpict);
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }


        public ActionResult GetNameSearch(string term)
        {
            var names = _pictureService.GetByName(term);
            return Json(names, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetPicturesNamePattern(int? page, string pattern)
        {
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            var listpict = Mapper.Map<IEnumerable<PictureEntity>, List<ViewPictureModel>>(await _pictureService.GetPicturesNamePatternAsync(pattern));
            return View("Index", listpict.ToPagedList(pageNumber, pageSize));
        }

        public async Task<ActionResult> ShowPicture(string tag)
        {
        string page = "https://api.imgur.com/3/gallery/search/?q=" + tag;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Client-ID dc3ac65d39aa159");
                var result = await client.GetAsync(page);
                var content = await result.Content.ReadAsAsync<RootObject>();
                var list = new List<string>();
                foreach (var item in content.data)
                {
                    if (item.images != null)
                    {
                        foreach (var i in item.images)
                        {
                            list.Add(i.link);
                        }
                    }
                }
                return View(list.Take(12).ToList());
            }
        }

        public ActionResult Download(string source)
        {
            try
            {
                HttpClient client = new HttpClient();
                byte[] data = client.GetByteArrayAsync(source).Result;
                var name = source.Split('/').Last();


                return File(data, "application/jpg", name);
            }
            catch (Exception err)
            {
                return new HttpNotFoundResult();
            }
        }


    }
}