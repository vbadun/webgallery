﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using DataAccess.Entities;

namespace Web.Controllers
{
    public class SettingController : Controller
    {
        private GalleryContext db = new GalleryContext();

        // GET: SettingEntities
        public ActionResult Index()
        {
            return View("Index",db.SettingEntities.ToList());
        }

        // GET: SettingEntities/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SettingEntity settingEntity = db.SettingEntities.Find(id);
            if (settingEntity == null)
            {
                return HttpNotFound();
            }
            return View(settingEntity);
        }

        // GET: SettingEntities/Create
        public ActionResult Create()
        {
            SelectList categories = new SelectList(db.Categories.ToList(), "Id", "Name");
            ViewBag.Categories = categories;
            return View();
        }

        // POST: SettingEntities/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(/*[Bind(Include = "Id,PicturesCount")]*/ SettingEntity settingEntity)
        {
            if (ModelState.IsValid)
            {
                settingEntity.Id = Guid.NewGuid();
                db.SettingEntities.Add(settingEntity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(settingEntity);
        }

        // GET: SettingEntities/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SettingEntity settingEntity = db.SettingEntities.Find(id);
            if (settingEntity == null)
            {
                return HttpNotFound();
            }
            return View(settingEntity);
        }

        // POST: SettingEntities/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PicturesCount")] SettingEntity settingEntity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(settingEntity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(settingEntity);
        }

        // GET: SettingEntities/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SettingEntity settingEntity = db.SettingEntities.Find(id);
            if (settingEntity == null)
            {
                return HttpNotFound();
            }
            return View(settingEntity);
        }

        // POST: SettingEntities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            SettingEntity settingEntity = db.SettingEntities.Find(id);
            db.SettingEntities.Remove(settingEntity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
