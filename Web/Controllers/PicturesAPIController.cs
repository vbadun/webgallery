﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccess;
using DataAccess.Entities;

namespace Web.Controllers
{
    public class PicturesAPIController : ApiController
    {
        private GalleryContext db = new GalleryContext();

        // GET: api/PicturesAPI
        public IQueryable<PictureEntity> GetPictures()
        {
            return db.Pictures;
        }

        // GET: api/PicturesAPI/5
        [ResponseType(typeof(PictureEntity))]
        public IHttpActionResult GetPictureEntity(Guid id)
        {
            PictureEntity pictureEntity = db.Pictures.Find(id);
            if (pictureEntity == null)
            {
                return NotFound();
            }

            return Ok(pictureEntity);
        }

        // PUT: api/PicturesAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPictureEntity(Guid id, PictureEntity pictureEntity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pictureEntity.Id)
            {
                return BadRequest();
            }

            db.Entry(pictureEntity).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PictureEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PicturesAPI
        [ResponseType(typeof(PictureEntity))]
        public IHttpActionResult PostPictureEntity(PictureEntity pictureEntity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pictures.Add(pictureEntity);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PictureEntityExists(pictureEntity.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pictureEntity.Id }, pictureEntity);
        }

        // DELETE: api/PicturesAPI/5
        [ResponseType(typeof(PictureEntity))]
        public IHttpActionResult DeletePictureEntity(Guid id)
        {
            PictureEntity pictureEntity = db.Pictures.Find(id);
            if (pictureEntity == null)
            {
                return NotFound();
            }

            db.Pictures.Remove(pictureEntity);
            db.SaveChanges();

            return Ok(pictureEntity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PictureEntityExists(Guid id)
        {
            return db.Pictures.Count(e => e.Id == id) > 0;
        }

    }
}