﻿using AutoMapper;
using DataAccess.Entities;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Http;
using System.Web.Routing;
using Web.Models;

namespace Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
     
            Mapper.Initialize(cfg => {
                cfg.CreateMap<PictureEntity, ViewPictureModel>()
                .ForMember("CategoryName", opt => opt.MapFrom(c => c.CategoryEntity.Name))
                .ForMember("Like", opt => opt.MapFrom(c => c.Raitings.Select(x => x.Like).Sum()))
                .ForMember("DisLike", opt => opt.MapFrom(c => c.Raitings.Select(x => x.DisLike).Sum()));

                cfg.CreateMap<CategoryEntity, CategoryList>()
                .ForMember("PicturesCount", opt => opt.MapFrom(c => c.Pictures.Count())); });

                      
        }
    }
}
