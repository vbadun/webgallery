﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ICommentService
    {
        void Add(CommentEntity item);
        IEnumerable<CommentEntity> GetAllByPictureId(Guid id);
    }
}
