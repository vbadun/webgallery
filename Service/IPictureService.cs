﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IPictureService
    {
        PictureEntity Get(Guid id);
        List<PictureEntity> GetAll();
        void Add(PictureEntity item);
        void Delete(Guid id);
        IEnumerable<string> GetByName(string term);
        Task <List<PictureEntity>> GetPicturesNamePatternAsync(string term);
        Task<List<PicturesByCategory>> GetByCount(int count);
    }
}
