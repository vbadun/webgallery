﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
   public class RaitingService : IRatingService
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public void Add(RaitingEntity item)
        {
            _unitOfWork.Raitings.Create(item);
            _unitOfWork.Commit();
        }

        public IEnumerable<RaitingEntity> GetByPictureId(Guid id)
        {
            return _unitOfWork.Raitings.GetRatingByPictureId(id);
        }

        //public PictureEntity Get(Guid id)
        //{
        //    return _unitOfWork.Pictures.Get(id);
        //}

        //public IEnumerable<PictureEntity> GetAll()
        //{
        //    return _unitOfWork.Pictures.GetAll();
        //}
    }
}

