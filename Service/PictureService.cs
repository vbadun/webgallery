﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Entities;

namespace Service
{
    public class PictureService : IPictureService
    {
       
     private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public void Add(PictureEntity item)
        {
            _unitOfWork.Pictures.Create(item);
            _unitOfWork.Commit();
        }

        public void Delete(Guid id)
        {
           _unitOfWork.Pictures.Delete(id);
            _unitOfWork.Commit();
        }

        public PictureEntity Get(Guid id)
        {
          return  _unitOfWork.Pictures.Get(id);
        }

        public List<PictureEntity> GetAll()
        {
            return _unitOfWork.Pictures.GetAllAsync().Result;
        }
        public async Task<List<PicturesByCategory>> GetByCount(int count)
        {
            return await _unitOfWork.Pictures.GetByCount(count);
        }


        public IEnumerable<string> GetByName(string term)
        {
            return _unitOfWork.Pictures.GetByName(term);
        }

        public async Task<List<PictureEntity>> GetPicturesNamePatternAsync(string term)
        {
          return await _unitOfWork.Pictures.GetPicturesNamePatternAsync(term);
        }
    }
}
