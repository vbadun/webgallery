﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class CommentService : ICommentService
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public void Add(CommentEntity item)
        {
            _unitOfWork.Comments.Create(item);
            _unitOfWork.Commit();
        }

        public IEnumerable<CommentEntity> GetAllByPictureId(Guid id)
        {
            return _unitOfWork.Comments.GetAllByPictureId(id);
        }

    }
}
