﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ICategoryService
    {
        CategoryEntity Get(Guid id);
        IEnumerable<CategoryEntity> GetAll();
        void Add(CategoryEntity item);
        void Delete(Guid id);
    }
}
