﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Entities;

namespace Service
{
    public class CategoryService : ICategoryService
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public void Add(CategoryEntity item)
        {
            _unitOfWork.Categories.Create(item);
            _unitOfWork.Commit();
        }

        public void Delete(Guid id)
        {
            _unitOfWork.Categories.Delete(id);
            _unitOfWork.Commit();
        }

        public CategoryEntity Get(Guid id)
        {
            return _unitOfWork.Categories.Get(id);
        }

        public IEnumerable<CategoryEntity> GetAll()
        {
            return _unitOfWork.Categories.GetAll();
        }
    }
}
