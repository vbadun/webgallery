﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IRatingService
    {
        void Add(RaitingEntity item);
        IEnumerable<RaitingEntity> GetByPictureId(Guid id);
    }
}
