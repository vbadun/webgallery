﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IRepository<TDomain> where TDomain : class
    {
        void Create(TDomain item);
        TDomain Get(Guid id);
        void Delete(Guid id);

       // Use for identity
        // List<TDomain> GetAll();
        Task<List<TDomain>> GetAllAsync();
        Task<List<TDomain>> GetAllAsync(CancellationToken cancellationToken);

        List<TDomain> PageAll(int skip, int take);
        Task<List<TDomain>> PageAllAsync(int skip, int take);
        Task<List<TDomain>> PageAllAsync(CancellationToken cancellationToken, int skip, int take);

        TDomain FindById(object id);
        Task<TDomain> FindByIdAsync(object id);
        Task<TDomain> FindByIdAsync(CancellationToken cancellationToken, object id);

        void Add(TDomain entity);
        void Update(TDomain entity);
        void Remove(TDomain entity);


    }
}
