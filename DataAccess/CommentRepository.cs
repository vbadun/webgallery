﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class CommentRepository
    {
        private GalleryContext _db;

        public CommentRepository(GalleryContext db)
        {
            _db = db;
        }
        public void Create(CommentEntity item)
        {
            _db.Comments.Add(item);

        }

        public IEnumerable<CommentEntity> GetAllByPictureId(Guid id)
        {
            return _db.Comments.Where(x => x.PictureEntityId == id);
        }
    }
}
