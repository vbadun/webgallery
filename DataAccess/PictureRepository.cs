﻿using DataAccess.Entities;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DataAccess
{
    public class PictureRepository : IRepository<PictureEntity>
    {
        private GalleryContext _db;

        public PictureRepository(GalleryContext db)
        {
            _db = db;
        }
        public void Create(PictureEntity item)
        {
            _db.Pictures.Add(item);
        }

        public PictureEntity Get(Guid id)
        {
            return _db.Pictures.Include(x => x.CategoryEntity).ToList().Find(x => x.Id == id);
        }

        public Task<List<PictureEntity>> GetAllAsync()
        {
            return  _db.Pictures.Include(x => x.CategoryEntity).Include(x => x.Raitings).ToListAsync();
        }

        public Task<List<PicturesByCategory>> GetByCount(int n)
        {
            var models = _db.Pictures.Include(x => x.CategoryEntity)
                .Include(x => x.Raitings)
                .GroupBy(x => x.CategoryEntity.Name)
                .Select(v => new PicturesByCategory
                {
                    CategoryName = v.Key,
                    Pictures = v.Take(n).ToList(),
                });
            return models.ToListAsync();
        }

        public void Delete(Guid id)
        {
            if (_db.Pictures.Find(id) != null)
            {
                var picture = _db.Pictures.Find(id);
                _db.Pictures.Remove(picture);
            }
        }

        public IEnumerable<string> GetByName(string term)
        {
            return _db.Pictures.Select(x => x.Name).Where(x => x.Contains(term.ToLower())).Take(100).ToList();
        }

        public async Task<List<PictureEntity>> GetPicturesNamePatternAsync(string term)
        {
            return await _db.Pictures.Where(x => x.Name.Contains(term.ToLower())).Take(100).ToListAsync();
        }

        public List<PictureEntity> PageAll(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public Task<List<PictureEntity>> PageAllAsync(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public Task<List<PictureEntity>> PageAllAsync(CancellationToken cancellationToken, int skip, int take)
        {
            throw new NotImplementedException();
        }

        public PictureEntity FindById(object id)
        {
            throw new NotImplementedException();
        }

        public Task<PictureEntity> FindByIdAsync(object id)
        {
            throw new NotImplementedException();
        }

        public Task<PictureEntity> FindByIdAsync(CancellationToken cancellationToken, object id)
        {
            throw new NotImplementedException();
        }

        public void Add(PictureEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(PictureEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(PictureEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task<List<PictureEntity>> GetAllAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
