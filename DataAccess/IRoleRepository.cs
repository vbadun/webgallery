﻿using WebGallery.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IRoleRepository : IRepository<Role>
    {
       // Task<Role> FindByNameAsync(string roleName, CancellationToken cancellationToken = default(CancellationToken));
        Role FindByName(string roleName);
        Task<Role> FindByNameAsync(string roleName);
        Task<Role> FindByNameAsync(CancellationToken cancellationToken, string roleName);
    }
}
