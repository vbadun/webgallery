﻿using System;

namespace DataAccess.Entities
{
    public class ClaimEntity
    {
        public Guid Id { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public Guid UserId { get; set; }
        public UserEntity UserEntity { get; set; }
    }
}
