﻿using System;
using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class RoleEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserEntity> Users { get; set; } = new List<UserEntity>();

    }
}
