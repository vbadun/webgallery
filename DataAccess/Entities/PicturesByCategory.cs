﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    
     public class PicturesByCategory
    {
        public string CategoryName { get; set; }

        public ICollection<PictureEntity> Pictures { get; set; }
    }
}
