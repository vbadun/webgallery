﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class RaitingEntity
    {
       
        public Guid Id { get; set; }

      
        public int? Like { get; set; }
        public int? DisLike { get; set; }

        public string HostAdress { get; set; }
        public Guid PictureEntityId { get; set; }
        public PictureEntity PictureEntity { get; set; }
        
    }
}
