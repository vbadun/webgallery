﻿using System;
using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class UserEntity
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }

        public virtual ICollection<ClaimEntity> Claims { get; set; } = new List<ClaimEntity>();
        public virtual ICollection<ExternalLoginEntity> Logins { get; set; } = new List<ExternalLoginEntity>();
        public virtual ICollection<RoleEntity> Roles { get; set; } = new List<RoleEntity>();

    }
}
