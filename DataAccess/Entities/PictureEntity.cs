﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class PictureEntity
    {
       
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string PathInfo { get; set; }

        public  Guid CategoryEntityId { get; set; }

        public  ICollection<RaitingEntity> Raitings { get; set; }
        public  ICollection<CommentEntity> Comments { get; set; }

        public  CategoryEntity CategoryEntity { get; set; }
      
    }
}
