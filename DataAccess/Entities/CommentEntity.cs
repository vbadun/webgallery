﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class CommentEntity
    {
        public Guid Id { get; set; }

        public string Comment { get; set; }

        public string HostAdress { get; set; }

        public DateTime DateTime { get; set; }
        public Guid PictureEntityId { get; set; }
      //  public Guid UserEntityId { get; set; }
        public PictureEntity PictureEntity { get; set; }
    }
}
