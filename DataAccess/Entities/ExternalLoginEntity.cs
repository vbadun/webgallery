﻿using System;

namespace DataAccess.Entities
{
    public class ExternalLoginEntity
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public Guid UserId { get; set; }
        public UserEntity UserEntity { get; set; }
    }
}
