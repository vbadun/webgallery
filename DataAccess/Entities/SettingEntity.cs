﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class SettingEntity
    {
        public Guid Id { get; set; }
        public ICollection<CategoryEntity> Categories { get; set; }
        public int PicturesCount { get; set; }
    }
}
