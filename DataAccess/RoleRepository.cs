﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using ExpressMapper.Extensions;
using WebGallery.Domain;

namespace DataAccess
{
    internal class RoleRepository : Repository<Role>, IRoleRepository
    {
        internal RoleRepository(GalleryContext context)
            : base(context)
        {
        }

        public Role FindByName(string roleName)
        {
            return Set.FirstOrDefault(x => x.Name == roleName);
        }

        public Task<Role> FindByNameAsync(string roleName)
        {
            return Set.FirstOrDefaultAsync(x => x.Name == roleName);
        }

        public Task<Role> FindByNameAsync(System.Threading.CancellationToken cancellationToken, string roleName)
        {
            return Set.FirstOrDefaultAsync(x => x.Name == roleName, cancellationToken);
        }
    }
}