﻿using DataAccess.Configurations;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class GalleryContext : DbContext
    {
        public GalleryContext() : base ("PictureContext")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserEntityConfiguration());
            modelBuilder.Configurations.Add(new RoleEntityConfiguration());
            modelBuilder.Configurations.Add(new ExternalLoginEntityConfiguration());
            modelBuilder.Configurations.Add(new ClaimEntityConfiguration());
            modelBuilder.Configurations.Add(new PictureConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new RaitingConfiguration());
            modelBuilder.Configurations.Add(new CommentConfiguration());
            modelBuilder.Configurations.Add(new SettingConfiguration());
        }
        public IDbSet<UserEntity> Users { get; set; }
        public IDbSet<RoleEntity> Roles { get; set; }
        public IDbSet<ExternalLoginEntity> Logins { get; set; }
        public IDbSet<ClaimEntity> Claims { get; set; }
        public DbSet<PictureEntity> Pictures { get; set; }
        public DbSet<CategoryEntity> Categories{ get; set; }
        public DbSet<RaitingEntity> Raitings { get; set; }
        public DbSet<CommentEntity> Comments { get; set; }

        public System.Data.Entity.DbSet<DataAccess.Entities.SettingEntity> SettingEntities { get; set; }
    }
}
