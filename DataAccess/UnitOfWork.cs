﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private GalleryContext db = new GalleryContext();
        private PictureRepository pictureRepository;
        private RaitingRepository raitingRepository;
        private CategoryRepository categoryRepository;
        private CommentRepository commentRepository;

        private IExternalLoginRepository _externalLoginRepository;
        private IRoleRepository _roleRepository;
        private IUserRepository _userRepository;

        public PictureRepository Pictures

        {
            get
            {
                if (pictureRepository == null)

                    pictureRepository = new PictureRepository(db);
                return pictureRepository;
            }
        }
        public CategoryRepository Categories

        {
            get
            {
                if (categoryRepository == null)

                    categoryRepository = new CategoryRepository(db);
                return categoryRepository;
            }
        }
        public RaitingRepository Raitings

        {
            get
            {
                if (raitingRepository == null)

                    raitingRepository = new RaitingRepository(db);
                return raitingRepository;
            }
        }
        public CommentRepository Comments
        {
            get
            {
                if (commentRepository == null)
                commentRepository = new CommentRepository(db);
                return commentRepository;
            }
        }

        public void Commit()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
            _externalLoginRepository = null;
            _roleRepository = null;
            _userRepository = null;
        }

        public Task<int> SaveChangesAsync()
        {
            return db.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return db.SaveChangesAsync(cancellationToken);
        }
       
        public IExternalLoginRepository ExternalLoginRepository
        {
            get { return _externalLoginRepository ?? (_externalLoginRepository = new ExternalLoginRepository(db)); }
        }

        public IRoleRepository RoleRepository
        {
            get { return _roleRepository ?? (_roleRepository = new RoleRepository(db)); }
        }

        public IUserRepository UserRepository
        {
            get { return _userRepository ?? (_userRepository = new UserRepository(db)); }
        }

        public int SaveChanges()
        {
            return db.SaveChanges();
        }
        public T GetRepository<T>()
        {
            throw new System.NotImplementedException();
        }


    }
}
