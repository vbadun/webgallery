﻿using DataAccess.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Configurations
{
    public class ClaimEntityConfiguration : EntityTypeConfiguration<ClaimEntity>
    {
        public ClaimEntityConfiguration()
        {
            ToTable("Claims");

            HasRequired(x => x.UserEntity)
                .WithMany(x => x.Claims)
                .HasForeignKey(x => x.UserId);
        }
    }
}