﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Configurations
{
    public class ExternalLoginEntityConfiguration : EntityTypeConfiguration<ExternalLoginEntity>
    {
        public ExternalLoginEntityConfiguration()
        {
            ToTable("ExternalLogins");

            HasKey(x => new { x.LoginProvider, x.ProviderKey, x.UserId });

            Property(x => x.LoginProvider)
                .HasMaxLength(128)
                .IsRequired();

            Property(x => x.ProviderKey)
                .HasMaxLength(128)
                .IsRequired();

            Property(x => x.UserId)
                .IsRequired();

            HasRequired(x => x.UserEntity)
                .WithMany(x => x.Logins)
                .HasForeignKey(x => x.UserId);
        }
    }
}