﻿
using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;


namespace DataAccess.Configurations
{
    public class PictureConfiguration : EntityTypeConfiguration<PictureEntity>
    {
        public PictureConfiguration()
        {
            this.ToTable("Pictures");
        }
    }
    public class CategoryConfiguration : EntityTypeConfiguration<CategoryEntity>
    {
        public CategoryConfiguration()
        {
            this.ToTable("Categories");
        }
    }
    public class RaitingConfiguration : EntityTypeConfiguration<RaitingEntity>
    {
        public RaitingConfiguration()
        {
            this.ToTable("Raitings");
        }
    }
    public class CommentConfiguration : EntityTypeConfiguration<CommentEntity>
    {
        public CommentConfiguration()
        {
            this.ToTable("Comments");
        }
    }
    
        public class SettingConfiguration : EntityTypeConfiguration<SettingEntity>
    {
        public SettingConfiguration()
        {
            this.ToTable("Settings");
        }
    }


}
