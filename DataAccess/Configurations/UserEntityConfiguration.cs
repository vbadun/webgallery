﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Configurations
{
    internal class UserEntityConfiguration : EntityTypeConfiguration<UserEntity>
    {
        internal UserEntityConfiguration()
        {
            ToTable("Users");

            Property(x => x.PasswordHash)
                .IsOptional();

            Property(x => x.SecurityStamp)
                .IsOptional();

            Property(x => x.UserName)
                .HasMaxLength(256)
                .IsRequired();

            HasMany(x => x.Roles)
                .WithMany(x => x.Users)
                .Map(x =>
                {
                    x.ToTable("User2Roles");
                    x.MapLeftKey("UserId");
                    x.MapRightKey("RoleId");
                });

            HasMany(x => x.Claims)
                .WithRequired(x => x.UserEntity)
                .HasForeignKey(x => x.UserId);

            HasMany(x => x.Logins)
                .WithRequired(x => x.UserEntity)
                .HasForeignKey(x => x.UserId);
        }
    }
}