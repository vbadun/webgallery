﻿using DataAccess.Entities;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class RaitingRepository
    {
        private GalleryContext _db;

        public RaitingRepository(GalleryContext db)
        {
            _db = db;
        }
        public void Create(RaitingEntity item)
        {
            _db.Raitings.Add(item);

        }

        public IEnumerable<RaitingEntity> GetRatingByPictureId(Guid id)
        {
            return _db.Raitings.Where(x => x.PictureEntityId == id);
        }

        //public RaitingEntity GetNegative(Guid id)
        //{
        //    return _db.Raitings.Include(x => x.CategoryEntity).ToList().Find(x => x.Id == id);
        //}



    }
}