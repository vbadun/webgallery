namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RatingTableCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Raitings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Like = c.Int(nullable: false),
                        DisLike = c.Int(nullable: false),
                        HostAdress = c.String(),
                        PictureEntityId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pictures", t => t.PictureEntityId, cascadeDelete: true)
                .Index(t => t.PictureEntityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Raitings", "PictureEntityId", "dbo.Pictures");
            DropIndex("dbo.Raitings", new[] { "PictureEntityId" });
            DropTable("dbo.Raitings");
        }
    }
}
