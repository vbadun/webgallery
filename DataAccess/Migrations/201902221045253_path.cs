namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class path : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pictures", "PathInfo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pictures", "PathInfo");
        }
    }
}
