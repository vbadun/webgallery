namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryIdToRictureTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pictures", "Category_Id", "dbo.Categories");
            DropIndex("dbo.Pictures", new[] { "Category_Id" });
            RenameColumn(table: "dbo.Pictures", name: "Category_Id", newName: "CategoryId");
            AlterColumn("dbo.Pictures", "CategoryId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Pictures", "CategoryId");
            AddForeignKey("dbo.Pictures", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pictures", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Pictures", new[] { "CategoryId" });
            AlterColumn("dbo.Pictures", "CategoryId", c => c.Guid());
            RenameColumn(table: "dbo.Pictures", name: "CategoryId", newName: "Category_Id");
            CreateIndex("dbo.Pictures", "Category_Id");
            AddForeignKey("dbo.Pictures", "Category_Id", "dbo.Categories", "Id");
        }
    }
}
