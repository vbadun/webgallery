namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DisLikeDeleted : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Raitings", "DisLike");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Raitings", "DisLike", c => c.Int());
        }
    }
}
