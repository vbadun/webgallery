namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCommentTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Comment = c.String(),
                        HostAdress = c.String(),
                        PictureEntityId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pictures", t => t.PictureEntityId, cascadeDelete: true)
                .Index(t => t.PictureEntityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "PictureEntityId", "dbo.Pictures");
            DropIndex("dbo.Comments", new[] { "PictureEntityId" });
            DropTable("dbo.Comments");
        }
    }
}
