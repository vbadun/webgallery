namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pictures", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Pictures", new[] { "CategoryId" });
            RenameColumn(table: "dbo.Pictures", name: "CategoryId", newName: "CategoryEntity_Id");
            AlterColumn("dbo.Pictures", "CategoryEntity_Id", c => c.Guid());
            CreateIndex("dbo.Pictures", "CategoryEntity_Id");
            AddForeignKey("dbo.Pictures", "CategoryEntity_Id", "dbo.Categories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pictures", "CategoryEntity_Id", "dbo.Categories");
            DropIndex("dbo.Pictures", new[] { "CategoryEntity_Id" });
            AlterColumn("dbo.Pictures", "CategoryEntity_Id", c => c.Guid(nullable: false));
            RenameColumn(table: "dbo.Pictures", name: "CategoryEntity_Id", newName: "CategoryId");
            CreateIndex("dbo.Pictures", "CategoryId");
            AddForeignKey("dbo.Pictures", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }
    }
}
