namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategories : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        PathInfo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Pictures", "Category_Id", c => c.Guid());
            CreateIndex("dbo.Pictures", "Category_Id");
            AddForeignKey("dbo.Pictures", "Category_Id", "dbo.Categories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pictures", "Category_Id", "dbo.Categories");
            DropIndex("dbo.Pictures", new[] { "Category_Id" });
            DropColumn("dbo.Pictures", "Category_Id");
            DropTable("dbo.Categories");
        }
    }
}
