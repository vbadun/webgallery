namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditCategories : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "CategoryName", c => c.String());
            DropColumn("dbo.Categories", "Name");
            DropColumn("dbo.Categories", "PathInfo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Categories", "PathInfo", c => c.String());
            AddColumn("dbo.Categories", "Name", c => c.String());
            DropColumn("dbo.Categories", "CategoryName");
        }
    }
}
