namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntitiLinksChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pictures", "CategoryId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pictures", "CategoryId");
        }
    }
}
