namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RatingTableCreateNULLAble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Raitings", "Like", c => c.Int());
            AlterColumn("dbo.Raitings", "DisLike", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Raitings", "DisLike", c => c.Int(nullable: false));
            AlterColumn("dbo.Raitings", "Like", c => c.Int(nullable: false));
        }
    }
}
