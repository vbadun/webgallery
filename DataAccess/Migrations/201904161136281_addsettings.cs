namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addsettings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PicturesCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Categories", "SettingEntity_Id", c => c.Guid());
            CreateIndex("dbo.Categories", "SettingEntity_Id");
            AddForeignKey("dbo.Categories", "SettingEntity_Id", "dbo.Settings", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Categories", "SettingEntity_Id", "dbo.Settings");
            DropIndex("dbo.Categories", new[] { "SettingEntity_Id" });
            DropColumn("dbo.Categories", "SettingEntity_Id");
            DropTable("dbo.Settings");
        }
    }
}
