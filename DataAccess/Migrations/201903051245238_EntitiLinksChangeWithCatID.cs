namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntitiLinksChangeWithCatID : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pictures", "CategoryEntity_Id", "dbo.Categories");
            DropIndex("dbo.Pictures", new[] { "CategoryEntity_Id" });
            RenameColumn(table: "dbo.Pictures", name: "CategoryEntity_Id", newName: "CategoryEntityId");
            AlterColumn("dbo.Pictures", "CategoryEntityId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Pictures", "CategoryEntityId");
            AddForeignKey("dbo.Pictures", "CategoryEntityId", "dbo.Categories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pictures", "CategoryEntityId", "dbo.Categories");
            DropIndex("dbo.Pictures", new[] { "CategoryEntityId" });
            AlterColumn("dbo.Pictures", "CategoryEntityId", c => c.Guid());
            RenameColumn(table: "dbo.Pictures", name: "CategoryEntityId", newName: "CategoryEntity_Id");
            CreateIndex("dbo.Pictures", "CategoryEntity_Id");
            AddForeignKey("dbo.Pictures", "CategoryEntity_Id", "dbo.Categories", "Id");
        }
    }
}
