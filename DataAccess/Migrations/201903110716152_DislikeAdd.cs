namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DislikeAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Raitings", "DisLike", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Raitings", "DisLike");
        }
    }
}
