﻿using DataAccess.Entities;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DataAccess
{
    public class CategoryRepository : IRepository<CategoryEntity>
    {

        private GalleryContext _db;

        public CategoryRepository(GalleryContext db)
        {
            _db = db;
        }
        public void Create(CategoryEntity item)
        {
            _db.Categories.Add(item);
           
        }

        public CategoryEntity Get(Guid id)
        {
            return _db.Categories.Find(id);
        }

        public IQueryable<CategoryEntity> GetAll()
        {
            return _db.Categories.Include(x=>x.Pictures);
            
        }
        public void Delete(Guid id)
        {
            if (_db.Categories.Find(id)!=null)
            {
                var category = _db.Categories.Find(id);
                _db.Categories.Remove(category);
            }
           
        }

        public Task<List<CategoryEntity>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<List<CategoryEntity>> GetAllAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public List<CategoryEntity> PageAll(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public Task<List<CategoryEntity>> PageAllAsync(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public Task<List<CategoryEntity>> PageAllAsync(CancellationToken cancellationToken, int skip, int take)
        {
            throw new NotImplementedException();
        }

        public CategoryEntity FindById(object id)
        {
            throw new NotImplementedException();
        }

        public Task<CategoryEntity> FindByIdAsync(object id)
        {
            throw new NotImplementedException();
        }

        public Task<CategoryEntity> FindByIdAsync(CancellationToken cancellationToken, object id)
        {
            throw new NotImplementedException();
        }

        public void Add(CategoryEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(CategoryEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(CategoryEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
