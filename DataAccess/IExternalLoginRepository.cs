﻿using DataAccess.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IExternalLoginRepository : IRepository<ExternalLoginEntity>
    {
        ExternalLoginEntity GetByProviderAndKey(string loginProvider, string providerKey);
        Task<ExternalLoginEntity> GetByProviderAndKeyAsync(string loginProvider, string providerKey);
        Task<ExternalLoginEntity> GetByProviderAndKeyAsync(CancellationToken cancellationToken, string loginProvider, string providerKey);
    }
}
